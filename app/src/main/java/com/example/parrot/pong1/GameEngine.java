package com.example.parrot.pong1;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.support.constraint.solver.widgets.Rectangle;
import android.util.Log;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;

import java.util.Random;

import java.util.ArrayList;

public class GameEngine extends SurfaceView implements Runnable {

    // -----------------------------------
    // ## ANDROID DEBUG VARIABLES
    // -----------------------------------

    // Android debug variables
    final static String TAG = "PONG-GAME";

    // -----------------------------------
    // ## SCREEN & DRAWING SETUP VARIABLES
    // -----------------------------------

    // screen size
    int screenHeight;
    int screenWidth;

    // game state
    boolean gameIsRunning;

    // threading
    Thread gameThread;


    // drawing variables
    SurfaceHolder holder;
    Canvas canvas;
    Paint paintbrush;


    int VISIBLE_LEFT;
    int VISIBLE_TOP;
    int VISIBLE_RIGHT;
    int VISIBLE_BOTTOM;
    int min_distance_from_wall = 100;
    int SQUARE_WIDTH = 30;

    // -----------------------------------
    // ## GAME SPECIFIC VARIABLES
    // -----------------------------------

    // ----------------------------
    // ## SPRITES
    // ----------------------------

    //player
    Player p;
    Square bullet1;
    ArrayList<Player> enemy = new ArrayList<Player>();
    ArrayList<Square> bullets = new ArrayList<Square>();

    int player_speed = 10;
    boolean player_moving_left = true;

    // ----------------------------
    // ## GAME STATS - number of lives, score, etc
    // ----------------------------

    int score = 0;
    int lives = 3;


    public GameEngine(Context context, int w, int h) {
        super(context);


        this.holder = this.getHolder();
        this.paintbrush = new Paint();

        this.screenWidth = w;
        this.screenHeight = h;

        this.VISIBLE_LEFT = 10;
        this.VISIBLE_TOP = 8;
        this.VISIBLE_RIGHT = this.screenWidth - 10;
        this.VISIBLE_BOTTOM = (int) (this.screenHeight * 0.87);

        this.printScreenInfo();

        // @TODO: Add your sprites to this section
        // This is optional. Use it to:
        //  - setup or configure your sprites
        //  - set the initial position of your sprites


        //add the player
        this.p = new Player(this.getContext(), screenWidth / 2 - 50, VISIBLE_BOTTOM - 150, R.drawable.player);


//        //add the enemies
//        this.enemy.add(new Player(this.getContext(), VISIBLE_LEFT, VISIBLE_TOP + 200, R.drawable.player));
//        this.enemy.add(new Player(this.getContext(), VISIBLE_LEFT + 200, VISIBLE_TOP + 200, R.drawable.player));
//        this.enemy.add(new Player(this.getContext(), VISIBLE_LEFT + 400, VISIBLE_TOP + 200, R.drawable.player));
//        this.enemy.add(new Player(this.getContext(), VISIBLE_LEFT + 600, VISIBLE_TOP + 200, R.drawable.player));
//        this.enemy.add(new Player(this.getContext(), VISIBLE_LEFT + 800, VISIBLE_TOP + 200, R.drawable.player));
//
//
//        //add the bullets
//        this.bullets.add(new Square(context, VISIBLE_LEFT, VISIBLE_TOP + 200, SQUARE_WIDTH));
//        this.bullets.add(new Square(context, VISIBLE_LEFT + 300, VISIBLE_TOP + 200, SQUARE_WIDTH));
//        this.bullets.add(new Square(context, VISIBLE_LEFT + 500, VISIBLE_TOP + 200, SQUARE_WIDTH));
//        this.bullets.add(new Square(context, VISIBLE_LEFT + 700, VISIBLE_TOP + 200, SQUARE_WIDTH));
//        this.bullets.add(new Square(context, VISIBLE_LEFT + 900, VISIBLE_TOP + 200, SQUARE_WIDTH));



        //draw single bullet

        bullet1=new Square(context,10,100,SQUARE_WIDTH);



    }

    // ------------------------------
    // HELPER FUNCTIONS
    // ------------------------------

    // This funciton prints the screen height & width to the screen.
    private void printScreenInfo() {

        Log.d(TAG, "Screen (w, h) = " + this.screenWidth + "," + this.screenHeight);
    }


    // ------------------------------
    // GAME STATE FUNCTIONS (run, stop, start)
    // ------------------------------
    @Override
    public void run() {
        while (gameIsRunning == true) {
            this.updatePositions();
            this.redrawSprites();
            this.setFPS();
        }
    }


    public void pauseGame() {
        gameIsRunning = false;
        try {
            gameThread.join();
        } catch (InterruptedException e) {
            // Error
        }
    }

    public void startGame() {
        gameIsRunning = true;
        gameThread = new Thread(this);
        gameThread.start();
    }


    // ------------------------------
    // GAME ENGINE FUNCTIONS
    // - update, draw, setFPS
    // ------------------------------

    // 1. Tell Android the (x,y) positions of your sprites
    public void updatePositions() {
        // @TODO: Update the position of the sprites

//        //make the player move left and right
//        if (player_moving_left == true) {
//            this.p.setxPosition(this.p.getxPosition() - player_speed);
//            //  this.p.getHitbox().left=this.p.getHitbox().left-player_speed;
//            // this.p.getHitbox().right=this.p.getHitbox().right-player_speed;
//
//
//        } else {
//            this.p.setxPosition(this.p.getxPosition() + player_speed);
//            //   this.p.getHitbox().left=this.p.getHitbox().left+player_speed;
//            //  this.p.getHitbox().right=this.p.getHitbox().right+player_speed;
//
//
//        }
//
//        if (this.p.getxPosition() < (this.VISIBLE_LEFT + min_distance_from_wall)) {
//            player_moving_left = false;
//        }
//        if (this.p.getxPosition() > this.VISIBLE_RIGHT - min_distance_from_wall) {
//            player_moving_left = true;
//        }
//
//
//
//
//        //make the bullets move
//        for (int i = 0; i < this.bullets.size(); i++) {
//            Square bull = this.bullets.get(i);
//
//            bull.setyPosition((bull.getyPosition() + 5));
//
//
//            bull.updateHitbox();
//        }

       //update the position of bullet
        bullet1.setyPosition(bullet1.getyPosition()+50);


//        // @TODO: Collision detection code
//
//        //if bullet from enemy touches the player,it losses its one live
//        for (int i = 0; i < this.bullets.size(); i++) {
//
//            Square bull = this.bullets.get(i);
//            if (bull.getHitbox().intersect(p.getHitbox())) {
//                this.lives = this.lives - 1;
//            }
//        }
    }

    // 2. Tell Android to DRAW the sprites at their positions


    public void redrawSprites() {
        if (this.holder.getSurface().isValid()) {
            this.canvas = this.holder.lockCanvas();

            //----------------
            // Put all your drawing code in this section

            // configure the drawing tools
            //  this.canvas.drawColor(Color.argb(255,0,0,255));
            // paintbrush.setColor(Color.WHITE);


            //@TODO: Draw the sprites (rectangle, circle, etc)

            //@TODO: Draw game statistics (lives, score, etc)


            //Draw the playfield area
            paintbrush.setStyle(Paint.Style.STROKE);
            paintbrush.setColor(Color.RED);
            canvas.drawRect(VISIBLE_LEFT, VISIBLE_TOP, VISIBLE_RIGHT, VISIBLE_BOTTOM, paintbrush);


            // 1. Draw the Score

            // Configure your paintbrush to be red and fill style
            paintbrush.setTextSize(60);
            paintbrush.setColor(Color.RED);
            paintbrush.setStyle(Paint.Style.FILL);
            canvas.drawText("Score: 25", VISIBLE_LEFT + 50, VISIBLE_TOP + 80, paintbrush);
            canvas.drawText("Lives: " + this.lives, VISIBLE_RIGHT - 250, VISIBLE_TOP + 80, paintbrush);

            // 2. Draw the player
//            canvas.drawBitmap(p.getImage(), this.p.getxPosition(), this.p.getyPosition(), paintbrush);


            // 3. Draw the player hitbox

            // Configure the paintbrush to be red & stroke style
            // We want it to be stroke because we want an OUTLINE around the image
//            paintbrush.setStyle(Paint.Style.STROKE);
//            paintbrush.setColor(Color.RED);
//            Rect r1 = p.getHitbox();
//            canvas.drawRect(r1, paintbrush);

//
//            // 4. Draw the enemies
//            for (int i = 0; i < this.enemy.size(); i++) {
//                Player e = this.enemy.get(i);
//                canvas.drawBitmap(e.getImage(), e.getxPosition(), e.getyPosition(), paintbrush);
//
//                //draw the enemies hitbox
//                paintbrush.setStyle(Paint.Style.STROKE);
//                paintbrush.setColor(Color.RED);
//                Rect r2 = e.getHitbox();
//                canvas.drawRect(r2, paintbrush);
//            }

//            //draw all the bullets
//            for (int i = 0; i < this.bullets.size(); i++) {
//
//
//                paintbrush.setStyle(Paint.Style.FILL);
//                paintbrush.setColor(Color.MAGENTA);
//                Square b = this.bullets.get(i);
//                int x = b.getxPosition();
//                int y = b.getyPosition();
//                canvas.drawRect(x, y, x + b.getWidth(), y + b.getWidth(), paintbrush);
//                paintbrush.setStyle(Paint.Style.STROKE);
//                paintbrush.setColor(Color.BLUE);
//                Rect r3 = b.getHitbox();
//                canvas.drawRect(r3, paintbrush);
//
//            }



            //Draw single bullet
            paintbrush.setStyle(Paint.Style.FILL);
            paintbrush.setColor(Color.BLUE);

            int left=this.bullet1.getxPosition();
            int top=this.bullet1.getyPosition();
            int right=this.bullet1.getxPosition()+SQUARE_WIDTH;
            int bottom=this.bullet1.getyPosition()+SQUARE_WIDTH;

            canvas.drawRect(left,top,right,bottom,paintbrush);


            //----------------
            this.holder.unlockCanvasAndPost(canvas);
        }
    }

    // Sets the frame rate of the game
    public void setFPS() {
        try {
            gameThread.sleep(17);
        } catch (Exception e) {

        }
    }

    // ------------------------------
    // USER INPUT FUNCTIONS
    // ------------------------------

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        int userAction = event.getActionMasked();
        //@TODO: What should happen when person touches the screen?
        if (userAction == MotionEvent.ACTION_DOWN) {
            // user pushed down on screen
        } else if (userAction == MotionEvent.ACTION_UP) {
            // user lifted their finger
        }
        return true;
    }
}